#telegram-desktop &
virtlogd -d &
light -U 0.8 &
nm-applet &
nitrogen --restore &
dropbox &
setxkbmap  -layout us,gr  -option grp:alt_shift_toggle &
dbus-launch &
#kmix &
#xcompmgr &
picom  &
#sleep 7s && pnmixer &
start-pulseaudio-x11 &
volumeicon &
cbatticon &
lxqt-policykit-agent &
sh -c "xrandr --setprovideroutputsource modesetting NVIDIA-0; xrandr --auto" &
slstatus
